﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using Aop;
using ExceptionHandle;
using DbFrame;
using DbFrame.Class;
using Utility;
using BLL;
using Model;
using AppControl.PageCode;

namespace HzyUI.Areas.Admin.Controllers.Sys
{
    public class RoleFunctionController : BaseController
    {
        // 角色功能
        // GET: /ManageSys/RoleFunction/
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            base.MenuID = "Z-140";
        }

        T_Roles troles = new T_Roles();
        T_Menu tmenu = new T_Menu();
        T_MenuBL tmenubl = new T_MenuBL();
        T_MenuFunction tmenufunction = new T_MenuFunction();
        T_Function tfunction = new T_Function();
        T_RoleMenuFunction trolemenufunction = new T_RoleMenuFunction();

        #region  基本操作，增删改查

        /// <summary>
        /// 获取角色菜单功能
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetRoleMenuFunctionTree(string roleid)
        {
            return Json(new { status = 1, value = tmenubl.GetRoleMenuFunctionTree(roleid) }, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// 保存角色功能
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Save(string json, string roleid)
        {
            if (Tools.getGuid(roleid).Equals(Guid.Empty))
                throw new MessageBox("请选择角色");
            var list = ((object[])jss.DeserializeObject(json)).ToList();

            if (!db.Delete<T_RoleMenuFunction>(w => w.uRoleMenuFunction_RoleID == roleid.To_Guid(), li))
                throw new MessageBox(db.ErrorMessge);
            if (list.Count > 0)
            {
                var guid_list = new List<Guid>();
                list.ForEach(item =>
                {
                    var func = (Dictionary<string, object>)item;
                    if (Tools.getString(func["tag"]).Equals("fun"))
                    {
                        if (db.Add<T_RoleMenuFunction>(new T_RoleMenuFunction()
                        {
                            uRoleMenuFunction_MenuID = func["pId"].To_Guid(),
                            uRoleMenuFunction_FunctionID = func["id"].To_Guid(),
                            uRoleMenuFunction_RoleID = roleid.To_Guid()
                        }, li).To_Guid().Equals(Guid.Empty))
                            throw new MessageBox(db.ErrorMessge);
                        guid_list.Add(Tools.getGuid(func["pId"]));
                    }
                });
            }
            if (!db.Commit(li))
                throw new MessageBox(db.ErrorMessge);
            return Json(new { status = 1 }, JsonRequestBehavior.DenyGet);
        }
        #endregion 基本操作，增删改查
    }
}
