﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using Aop;
using ExceptionHandle;
using DbFrame;
using DbFrame.Class;
using Utility;
using BLL;
using Model;
using AppControl.PageCode;

namespace HzyUI.Areas.Admin.Controllers.Sys
{
    public class MenuFunctionController : BaseController
    {
        // 菜单功能
        // GET: /ManageSys/MenuFunction/
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            base.MenuID = "Z-130";
        }

        T_Menu tmenu = new T_Menu();
        T_MenuBL tmenubl = new T_MenuBL();
        T_Function tfunction = new T_Function();
        T_MenuFunction tmenufunction = new T_MenuFunction();


        #region  查询数据列表
        /// <summary>
        /// 获取列表数据
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [NonAction]
        public override PagingEntity GetPagingEntity(FormCollection fc, int page = 1, int rows = 20)
        {
            //检索
            var query = this.FormCollectionToHashtable(fc);
            //获取列表
            return tmenubl.GetDataSource(query, page, rows);
        }
        #endregion  查询数据列表

        /// <summary>
        /// 获取菜单和功能树
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetMenuAndFunctionTree()
        {
            return Json(new { status = 1, value = tmenubl.GetMenuAndFunctionTree() }, JsonRequestBehavior.DenyGet);
        }




        #region  基本操作，增删改查
        /// <summary>
        /// 保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Save(T_Menu model)
        {
            tmenu = new T_Menu();
            tmenu = model;
            if (model.uMenu_ID.To_Guid() == Guid.Empty)
            {
                this.KeyID = Tools.getGuidString(db.Add(tmenu, li));
                if (Tools.getGuid(KeyID).Equals(Guid.Empty))
                    throw new MessageBox(db.ErrorMessge);
            }
            else
            {
                this.KeyID = Tools.getGuidString(model.uMenu_ID);
                if (!db.Edit<T_Menu>(tmenu, w => w.uMenu_ID == tmenu.uMenu_ID, li))
                    throw new MessageBox(db.ErrorMessge);
            }

            //删除菜单的功能
            if (!db.Delete<T_MenuFunction>(w => w.uMenuFunction_MenuID == KeyID.To_Guid(), li))
                throw new MessageBox(db.ErrorMessge);
            var uFunction_ID = Request.Form["uFunction_ID[]"].To_String().Split(',').ToList();
            uFunction_ID.ForEach(item =>
            {
                var funcid = db.Add(new T_MenuFunction()
                {
                    uMenuFunction_MenuID = KeyID.To_Guid(),
                    uMenuFunction_FunctionID = item.To_Guid(),
                }, li);
                if (funcid.To_Guid() == Guid.Empty) throw new MessageBox(db.ErrorMessge);
            });

            if (!db.Commit(li))
                throw new MessageBox(db.ErrorMessge);
            return Json(new { status = 1, ID = KeyID }, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Del(string ID)
        {
            db.JsonToList<string>(ID).ForEach(item =>
            {
                //删除菜单的功能
                if (!db.Delete<T_MenuFunction>(w => w.uMenuFunction_MenuID == item.To_Guid(), li))
                    throw new MessageBox(db.ErrorMessge);

                if (!db.Delete<T_Menu>(w => w.uMenu_ID == item.To_Guid(), li))
                    throw new MessageBox(db.ErrorMessge);
            });

            if (!db.Commit(li))
                throw new MessageBox(db.ErrorMessge);
            return Json(new { status = 1 }, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// 查询根据ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Find(string ID)
        {
            tmenu = db.Find<T_Menu>(w => w.uMenu_ID == Tools.getGuid(ID));

            if (Tools.getGuid(ID).Equals(Guid.Empty))
            {
                tmenu.dMenu_CreateTime = Tools.getDateTime(DateTime.Now);
            }

            var menu = db.Find<T_Menu>(w => w.uMenu_ID == tmenu.uMenu_ParentID);

            var Menu_Power = db.FindToList<T_MenuFunction>(w => w.uMenuFunction_MenuID == ID.To_Guid()).Select(item => item.uMenuFunction_FunctionID);

            var di = new ToJson().GetDictionary(new Dictionary<string, object>()
            {
                {"tmenu",tmenu},
                {"pname",menu.cMenu_Name.To_String()},
                {"Menu_Power",Menu_Power},
                {"status",1}
            });
            di["dMenu_CreateTime"] = Tools.getDateTimeString(di["dMenu_CreateTime"], "yyyy-MM-dd HH:mm:ss");
            return Json(di, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// 保存菜单功能
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveMenuFunction(string nodes)
        {
            var json = ((object[])jss.DeserializeObject(nodes)).ToList();
            var list = new List<Guid>();
            if (!db.Delete<T_MenuFunction>(null, li))
                throw new MessageBox(db.ErrorMessge);
            json.ForEach(item =>
            {
                var func = (Dictionary<string, object>)item;
                if (Tools.getString(func["tag"]).Equals("fun"))
                {
                    var menuid = list.Find(x => x.Equals(Tools.getGuid(func["pId"])));
                    if (Tools.getGuid(db.Add<T_MenuFunction>(new T_MenuFunction()
                    {
                        uMenuFunction_MenuID = func["pId"].To_Guid(),
                        uMenuFunction_FunctionID = func["id"].To_Guid()
                    }, li)).Equals(Guid.Empty))
                        throw new MessageBox(db.ErrorMessge);
                    list.Add(Tools.getGuid(func["pId"]));
                }
            });
            if (!db.Commit(li))
                throw new MessageBox(db.ErrorMessge);
            return Json(new { status = 1 }, JsonRequestBehavior.DenyGet);
        }
        #endregion  基本操作，增删改查

    }
}
