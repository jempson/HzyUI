﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aop
{
    public class ClassConfig
    {
        /// <summary>
        /// 错误页面地址
        /// </summary>
        public static string ErrorPageUrl = "~/Areas/Admin/Views/Error/Index.cshtml";

        /// <summary>
        /// 登录地址
        /// </summary>
        public static string LoginPageUrl = "/Home/Index/";

        /// <summary>
        /// 首页地址
        /// </summary>
        public static string HomePageUrl = "/Admin/Home/Index/";
    }
}