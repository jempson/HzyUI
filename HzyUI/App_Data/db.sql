USE [Web]
GO
/****** Object:  Table [dbo].[Member]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Member](
	[Member_ID] [uniqueidentifier] NOT NULL,
	[Member_Name] [nvarchar](50) NULL,
	[Member_Sex] [nvarchar](2) NULL,
	[Member_CreateTime] [datetime] NULL,
 CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED 
(
	[Member_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_Function]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Function](
	[uFunction_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_T_Function_uFunction_ID]  DEFAULT (newid()),
	[iFunction_Number] [int] NULL,
	[cFunction_Name] [varchar](50) NULL,
	[cFunction_ByName] [varchar](50) NULL,
	[dFunction_CreateTime] [datetime] NULL CONSTRAINT [DF_T_Function_dFunction_CreateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_T_Function] PRIMARY KEY CLUSTERED 
(
	[uFunction_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Menu]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Menu](
	[uMenu_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_T_Menu_uMenu_ID]  DEFAULT (newid()),
	[cMenu_Name] [varchar](50) NULL,
	[cMenu_Url] [varchar](50) NULL,
	[uMenu_ParentID] [uniqueidentifier] NULL,
	[cMenu_Number] [varchar](50) NULL,
	[cMenu_ICON] [varchar](50) NULL,
	[dMenu_CreateTime] [datetime] NULL CONSTRAINT [DF_T_Menu_dMenu_CreateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_T_Menu] PRIMARY KEY CLUSTERED 
(
	[uMenu_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_MenuFunction]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_MenuFunction](
	[uMenuFunction_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_T_MenuFunction_uMenuFunction_ID]  DEFAULT (newid()),
	[uMenuFunction_MenuID] [uniqueidentifier] NULL,
	[uMenuFunction_FunctionID] [uniqueidentifier] NULL,
	[dMenuFunction_CreateTime] [datetime] NULL CONSTRAINT [DF_T_MenuFunction_dMenuFunction_CreateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_T_MenuFunction] PRIMARY KEY CLUSTERED 
(
	[uMenuFunction_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_Number]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Number](
	[Number_ID] [uniqueidentifier] NOT NULL,
	[Number_Num] [varchar](50) NULL,
	[Number_DataBase] [varchar](50) NULL,
	[Number_TableName] [varchar](50) NULL,
	[Number_NumField] [varchar](50) NULL,
	[Number_CreateTime] [datetime] NULL,
 CONSTRAINT [PK_T_Number] PRIMARY KEY CLUSTERED 
(
	[Number_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_RoleMenuFunction]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_RoleMenuFunction](
	[uRoleMenuFunction_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_T_RoleMenuFunction_uRoleMenuFunction_ID]  DEFAULT (newid()),
	[uRoleMenuFunction_RoleID] [uniqueidentifier] NULL,
	[uRoleMenuFunction_FunctionID] [uniqueidentifier] NULL,
	[uRoleMenuFunction_MenuID] [uniqueidentifier] NULL,
	[dRoleMenuFunction_CreateTime] [datetime] NULL CONSTRAINT [DF_T_RoleMenuFunction_dRoleMenuFunction_CreateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_T_RoleMenuFunction] PRIMARY KEY CLUSTERED 
(
	[uRoleMenuFunction_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_Roles]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Roles](
	[uRoles_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Roles_Roles_ID]  DEFAULT (newid()),
	[cRoles_Number] [varchar](50) NULL,
	[cRoles_Name] [varchar](50) NULL,
	[cRoles_Remark] [varchar](500) NULL,
	[dRoles_CreateTime] [datetime] NULL CONSTRAINT [DF_Roles_Roles_CreateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[uRoles_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Users]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Users](
	[uUsers_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_T_Users_Users_ID]  DEFAULT (newid()),
	[cUsers_Name] [varchar](50) NULL,
	[cUsers_LoginName] [varchar](50) NULL,
	[cUsers_LoginPwd] [varchar](100) NULL,
	[cUsers_Email] [varchar](50) NULL,
	[dUsers_CreateTime] [datetime] NULL CONSTRAINT [DF_T_Users_Users_CreateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_T_Users] PRIMARY KEY CLUSTERED 
(
	[uUsers_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_UsersRoles]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_UsersRoles](
	[uUsersRoles_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_T_UsersRoles_uUsersRole_ID]  DEFAULT (newid()),
	[uUsersRoles_UsersID] [uniqueidentifier] NULL,
	[uUsersRoles_RoleID] [uniqueidentifier] NULL,
	[dUsersRoles_CreateTime] [datetime] NULL CONSTRAINT [DF_T_UsersRoles_dUsersRole_CreateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_T_UsersRoles] PRIMARY KEY CLUSTERED 
(
	[uUsersRoles_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[T_Function] ([uFunction_ID], [iFunction_Number], [cFunction_Name], [cFunction_ByName], [dFunction_CreateTime]) VALUES (N'b6fd5425-504a-46a9-993b-2f8dc9158eb8', 80, N'打印', N'Print', CAST(N'2016-06-20 13:40:36.787' AS DateTime))
INSERT [dbo].[T_Function] ([uFunction_ID], [iFunction_Number], [cFunction_Name], [cFunction_ByName], [dFunction_CreateTime]) VALUES (N'c9518758-b2e1-4f51-b517-5282e273889c', 10, N'能否拥有该菜单', N'Have', CAST(N'2016-06-20 13:40:29.657' AS DateTime))
INSERT [dbo].[T_Function] ([uFunction_ID], [iFunction_Number], [cFunction_Name], [cFunction_ByName], [dFunction_CreateTime]) VALUES (N'f27ecb0a-197d-47b1-b243-59a8c71302bf', 60, N'检索', N'Search', CAST(N'2017-02-16 17:06:23.430' AS DateTime))
INSERT [dbo].[T_Function] ([uFunction_ID], [iFunction_Number], [cFunction_Name], [cFunction_ByName], [dFunction_CreateTime]) VALUES (N'383e7ee2-7690-46ac-9230-65155c84aa30', 50, N'保存', N'Save', CAST(N'2017-04-22 13:51:52.837' AS DateTime))
INSERT [dbo].[T_Function] ([uFunction_ID], [iFunction_Number], [cFunction_Name], [cFunction_ByName], [dFunction_CreateTime]) VALUES (N'9c388461-2704-4c5e-a729-72c17e9018e1', 40, N'删除', N'Del', CAST(N'2016-06-20 13:40:52.360' AS DateTime))
INSERT [dbo].[T_Function] ([uFunction_ID], [iFunction_Number], [cFunction_Name], [cFunction_ByName], [dFunction_CreateTime]) VALUES (N'bffefb1c-8988-4ddf-b4ac-81c2b30e80cd', 20, N'添加', N'Add', CAST(N'2016-06-20 13:40:36.787' AS DateTime))
INSERT [dbo].[T_Function] ([uFunction_ID], [iFunction_Number], [cFunction_Name], [cFunction_ByName], [dFunction_CreateTime]) VALUES (N'2401f4d0-60b0-4e2e-903f-84c603373572', 70, N'导出', N'GetExcel', CAST(N'2017-02-09 16:34:14.017' AS DateTime))
INSERT [dbo].[T_Function] ([uFunction_ID], [iFunction_Number], [cFunction_Name], [cFunction_ByName], [dFunction_CreateTime]) VALUES (N'e7ef2a05-8317-41c3-b588-99519fe88bf9', 30, N'修改', N'Edit', CAST(N'2016-06-20 13:40:43.153' AS DateTime))
INSERT [dbo].[T_Menu] ([uMenu_ID], [cMenu_Name], [cMenu_Url], [uMenu_ParentID], [cMenu_Number], [cMenu_ICON], [dMenu_CreateTime]) VALUES (N'd809d891-011d-46e0-9501-1497043f2139', N'系统管理', NULL, NULL, N'Z', N'fa fa-desktop', CAST(N'2016-06-17 09:29:40.893' AS DateTime))
INSERT [dbo].[T_Menu] ([uMenu_ID], [cMenu_Name], [cMenu_Url], [uMenu_ParentID], [cMenu_Number], [cMenu_ICON], [dMenu_CreateTime]) VALUES (N'27f43299-e238-4c9a-b4d0-55ea1c6c2b4b', N'修改密码', N'/Admin/ChangePwd/Index', N'd809d891-011d-46e0-9501-1497043f2139', N'Z-150', NULL, CAST(N'2016-06-20 13:34:24.710' AS DateTime))
INSERT [dbo].[T_Menu] ([uMenu_ID], [cMenu_Name], [cMenu_Url], [uMenu_ParentID], [cMenu_Number], [cMenu_ICON], [dMenu_CreateTime]) VALUES (N'26262205-e5b2-4b04-a1fe-7c7ae3d3b68b', N'代码创建', N'/Admin/CreateCode/Index', N'd809d891-011d-46e0-9501-1497043f2139', N'Z-160', NULL, CAST(N'2017-04-15 15:48:02.000' AS DateTime))
INSERT [dbo].[T_Menu] ([uMenu_ID], [cMenu_Name], [cMenu_Url], [uMenu_ParentID], [cMenu_Number], [cMenu_ICON], [dMenu_CreateTime]) VALUES (N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', N'用户管理', N'/Admin/User/Index', N'd809d891-011d-46e0-9501-1497043f2139', N'Z-100', NULL, CAST(N'2016-06-17 09:30:59.270' AS DateTime))
INSERT [dbo].[T_Menu] ([uMenu_ID], [cMenu_Name], [cMenu_Url], [uMenu_ParentID], [cMenu_Number], [cMenu_ICON], [dMenu_CreateTime]) VALUES (N'012365a8-3a84-409e-ac50-b1b5bb9977bf', N'功能管理', N'/Admin/Function/Index', N'd809d891-011d-46e0-9501-1497043f2139', N'Z-120', NULL, CAST(N'2017-04-15 10:28:32.000' AS DateTime))
INSERT [dbo].[T_Menu] ([uMenu_ID], [cMenu_Name], [cMenu_Url], [uMenu_ParentID], [cMenu_Number], [cMenu_ICON], [dMenu_CreateTime]) VALUES (N'b326c7fa-464f-4901-a32f-b4ee67d2c9be', N'菜单功能', N'/Admin/MenuFunction/Index', N'd809d891-011d-46e0-9501-1497043f2139', N'Z-130', NULL, CAST(N'2016-06-20 10:21:43.587' AS DateTime))
INSERT [dbo].[T_Menu] ([uMenu_ID], [cMenu_Name], [cMenu_Url], [uMenu_ParentID], [cMenu_Number], [cMenu_ICON], [dMenu_CreateTime]) VALUES (N'46668a22-f006-4738-8531-c4821cc98802', N'角色功能', N'/Admin/RoleFunction/Index', N'd809d891-011d-46e0-9501-1497043f2139', N'Z-140', NULL, CAST(N'2016-06-20 13:35:16.850' AS DateTime))
INSERT [dbo].[T_Menu] ([uMenu_ID], [cMenu_Name], [cMenu_Url], [uMenu_ParentID], [cMenu_Number], [cMenu_ICON], [dMenu_CreateTime]) VALUES (N'351dd2bc-23a0-4351-b155-d028db5bf980', N'角色管理', N'/Admin/Role/Index', N'd809d891-011d-46e0-9501-1497043f2139', N'Z-110', NULL, CAST(N'2016-06-20 09:47:54.453' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'cd436576-0a42-4a84-acd7-0819e8a3369e', N'46668a22-f006-4738-8531-c4821cc98802', N'c9518758-b2e1-4f51-b517-5282e273889c', CAST(N'2017-06-15 10:36:32.090' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'9d4268d3-0d28-47ea-b549-0e16e04e46a4', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', N'bffefb1c-8988-4ddf-b4ac-81c2b30e80cd', CAST(N'2017-09-03 19:18:35.150' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'4dc7b420-0d98-496b-afc8-0e33db024670', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', N'c9518758-b2e1-4f51-b517-5282e273889c', CAST(N'2017-09-03 19:18:35.150' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'5d0f48b9-64fb-47d6-9f91-2652bf8a9293', N'46668a22-f006-4738-8531-c4821cc98802', N'bffefb1c-8988-4ddf-b4ac-81c2b30e80cd', CAST(N'2017-06-15 10:36:32.090' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'72bcfdf7-80c2-41d2-9e6d-35b42a7c1bb3', N'351dd2bc-23a0-4351-b155-d028db5bf980', N'bffefb1c-8988-4ddf-b4ac-81c2b30e80cd', CAST(N'2017-06-15 10:36:32.087' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'2fbcc1ce-ce27-4be7-a7c7-3aba57682373', N'351dd2bc-23a0-4351-b155-d028db5bf980', N'e7ef2a05-8317-41c3-b588-99519fe88bf9', CAST(N'2017-06-15 10:36:32.087' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'8f36e0fb-9ba8-46e4-ae54-3bedfd3636e5', N'46668a22-f006-4738-8531-c4821cc98802', N'e7ef2a05-8317-41c3-b588-99519fe88bf9', CAST(N'2017-06-15 10:36:32.090' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'881732c8-f675-4b6d-abfe-3e4d2cbb3ad3', N'351dd2bc-23a0-4351-b155-d028db5bf980', N'2401f4d0-60b0-4e2e-903f-84c603373572', CAST(N'2017-06-15 10:36:32.090' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'2175c23c-32cb-4a22-8807-3f9ec5724acb', N'351dd2bc-23a0-4351-b155-d028db5bf980', N'383e7ee2-7690-46ac-9230-65155c84aa30', CAST(N'2017-06-15 10:36:32.087' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'233a38a1-ba23-4c24-9ce7-41203b055410', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', N'f27ecb0a-197d-47b1-b243-59a8c71302bf', CAST(N'2017-09-03 19:18:35.150' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'd7c17736-999f-461f-91c7-471df6298c27', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', N'e7ef2a05-8317-41c3-b588-99519fe88bf9', CAST(N'2017-09-03 19:18:35.150' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'6e6ab975-09b3-4e2a-b6b2-58a088eda1b9', N'351dd2bc-23a0-4351-b155-d028db5bf980', N'9c388461-2704-4c5e-a729-72c17e9018e1', CAST(N'2017-06-15 10:36:32.087' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'c07e22b2-daaf-4279-a547-5ef66758a8c2', N'27f43299-e238-4c9a-b4d0-55ea1c6c2b4b', N'c9518758-b2e1-4f51-b517-5282e273889c', CAST(N'2017-06-15 10:36:32.090' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'ba24c9c2-6561-4611-8c82-6a1efc589f7e', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', N'9c388461-2704-4c5e-a729-72c17e9018e1', CAST(N'2017-09-03 19:18:35.150' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'44815d0a-5b05-472f-bf2b-6c589b42a2ea', N'd809d891-011d-46e0-9501-1497043f2139', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-09-09 22:49:18.197' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'f6af003a-63ea-41ce-9b7e-7d02df9abc08', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', N'383e7ee2-7690-46ac-9230-65155c84aa30', CAST(N'2017-09-03 19:18:35.150' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'ab2c3a10-afdd-4c11-905e-970a86c839a3', N'46668a22-f006-4738-8531-c4821cc98802', N'9c388461-2704-4c5e-a729-72c17e9018e1', CAST(N'2017-06-15 10:36:32.090' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'a113123b-b1a5-4a7f-bc64-cb0b3005493b', N'351dd2bc-23a0-4351-b155-d028db5bf980', N'c9518758-b2e1-4f51-b517-5282e273889c', CAST(N'2017-06-15 10:36:32.087' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'3942615f-274f-42da-8c7a-db963e42789c', N'351dd2bc-23a0-4351-b155-d028db5bf980', N'f27ecb0a-197d-47b1-b243-59a8c71302bf', CAST(N'2017-06-15 10:36:32.090' AS DateTime))
INSERT [dbo].[T_MenuFunction] ([uMenuFunction_ID], [uMenuFunction_MenuID], [uMenuFunction_FunctionID], [dMenuFunction_CreateTime]) VALUES (N'09a702f9-7411-4276-a5d9-f41858f98c29', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', N'2401f4d0-60b0-4e2e-903f-84c603373572', CAST(N'2017-09-03 19:18:35.150' AS DateTime))
INSERT [dbo].[T_RoleMenuFunction] ([uRoleMenuFunction_ID], [uRoleMenuFunction_RoleID], [uRoleMenuFunction_FunctionID], [uRoleMenuFunction_MenuID], [dRoleMenuFunction_CreateTime]) VALUES (N'f60b1d8b-d786-43b1-bd90-017f66b378e2', N'18fa4771-6f58-46a3-80d2-6f0f5e9972e3', N'9c388461-2704-4c5e-a729-72c17e9018e1', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', CAST(N'2017-09-03 20:21:34.650' AS DateTime))
INSERT [dbo].[T_RoleMenuFunction] ([uRoleMenuFunction_ID], [uRoleMenuFunction_RoleID], [uRoleMenuFunction_FunctionID], [uRoleMenuFunction_MenuID], [dRoleMenuFunction_CreateTime]) VALUES (N'77243d64-379c-4254-8101-2d018988649a', N'18fa4771-6f58-46a3-80d2-6f0f5e9972e3', N'c9518758-b2e1-4f51-b517-5282e273889c', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', CAST(N'2017-09-03 20:21:34.650' AS DateTime))
INSERT [dbo].[T_RoleMenuFunction] ([uRoleMenuFunction_ID], [uRoleMenuFunction_RoleID], [uRoleMenuFunction_FunctionID], [uRoleMenuFunction_MenuID], [dRoleMenuFunction_CreateTime]) VALUES (N'7dc29596-be75-4251-ba06-3d0d36ef2f0a', N'18fa4771-6f58-46a3-80d2-6f0f5e9972e3', N'383e7ee2-7690-46ac-9230-65155c84aa30', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', CAST(N'2017-09-03 20:21:34.650' AS DateTime))
INSERT [dbo].[T_RoleMenuFunction] ([uRoleMenuFunction_ID], [uRoleMenuFunction_RoleID], [uRoleMenuFunction_FunctionID], [uRoleMenuFunction_MenuID], [dRoleMenuFunction_CreateTime]) VALUES (N'6a41ca2c-a20d-4150-a61e-7002d256e8a2', N'18fa4771-6f58-46a3-80d2-6f0f5e9972e3', N'2401f4d0-60b0-4e2e-903f-84c603373572', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', CAST(N'2017-09-03 20:21:34.650' AS DateTime))
INSERT [dbo].[T_RoleMenuFunction] ([uRoleMenuFunction_ID], [uRoleMenuFunction_RoleID], [uRoleMenuFunction_FunctionID], [uRoleMenuFunction_MenuID], [dRoleMenuFunction_CreateTime]) VALUES (N'48616c9e-38fe-4f21-9a68-7c68b1a641be', N'18fa4771-6f58-46a3-80d2-6f0f5e9972e3', N'bffefb1c-8988-4ddf-b4ac-81c2b30e80cd', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', CAST(N'2017-09-03 20:21:34.650' AS DateTime))
INSERT [dbo].[T_RoleMenuFunction] ([uRoleMenuFunction_ID], [uRoleMenuFunction_RoleID], [uRoleMenuFunction_FunctionID], [uRoleMenuFunction_MenuID], [dRoleMenuFunction_CreateTime]) VALUES (N'50b6e20e-8cd8-4f25-81ad-92fdc4569d3b', N'18fa4771-6f58-46a3-80d2-6f0f5e9972e3', N'e7ef2a05-8317-41c3-b588-99519fe88bf9', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', CAST(N'2017-09-03 20:21:34.650' AS DateTime))
INSERT [dbo].[T_RoleMenuFunction] ([uRoleMenuFunction_ID], [uRoleMenuFunction_RoleID], [uRoleMenuFunction_FunctionID], [uRoleMenuFunction_MenuID], [dRoleMenuFunction_CreateTime]) VALUES (N'102a9f85-23cf-47ff-b18d-c16073ea357f', N'18fa4771-6f58-46a3-80d2-6f0f5e9972e3', N'f27ecb0a-197d-47b1-b243-59a8c71302bf', N'f5ca4bbb-9d71-4ac2-99a1-868569f4a0e8', CAST(N'2017-09-03 20:21:34.650' AS DateTime))
INSERT [dbo].[T_Roles] ([uRoles_ID], [cRoles_Number], [cRoles_Name], [cRoles_Remark], [dRoles_CreateTime]) VALUES (N'18fa4771-6f58-46a3-80d2-6f0f5e9972e3', N'0001', N'管理员', N'拥有所有权限', CAST(N'2016-06-20 10:20:10.073' AS DateTime))
INSERT [dbo].[T_Roles] ([uRoles_ID], [cRoles_Number], [cRoles_Name], [cRoles_Remark], [dRoles_CreateTime]) VALUES (N'40ff1844-c099-4061-98e0-cd6e544fcfd3', N'0002', N'普通用户', NULL, CAST(N'2016-07-06 17:59:20.527' AS DateTime))
INSERT [dbo].[T_Users] ([uUsers_ID], [cUsers_Name], [cUsers_LoginName], [cUsers_LoginPwd], [cUsers_Email], [dUsers_CreateTime]) VALUES (N'0198459e-2034-4533-b843-5d227ad20740', N'管理员', N'admin', N'123', N'1396510655@qq.com', CAST(N'2017-04-06 19:55:53.083' AS DateTime))
INSERT [dbo].[T_Users] ([uUsers_ID], [cUsers_Name], [cUsers_LoginName], [cUsers_LoginPwd], [cUsers_Email], [dUsers_CreateTime]) VALUES (N'a7eae7ab-0de4-4026-8da9-6529f8a1c3e2', N'普通用户', N'user', N'123', NULL, CAST(N'2017-04-22 13:44:44.983' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'bc1a0f0b-17c6-4389-97e6-0694d2b7fd04', N'c7746048-193a-495b-b224-be7cd60eae48', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:57:33.020' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'e40dbb16-da1b-44c0-873a-08db1e093d26', N'c4a4ef15-3033-4204-8fe4-99951cfa0a4d', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:28:01.700' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'b3f119aa-648a-4a5a-81f8-1532b64f1b37', N'f5a4663e-561a-4a61-951b-1ead3e24d33f', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:39:30.313' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'ef49215e-065c-4b35-9bf6-20215124760b', N'28509bb9-f041-4a05-a2ae-8828dde126af', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:54:07.293' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'a6eddbb3-fc6d-437f-a540-34ed09fbd1be', N'ec69a2e1-c61f-4f45-bab4-55ffb3fe25a9', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:37:48.900' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'3d9e63f7-0aa9-47b8-9bb0-3551f38f7591', N'18426e34-c39b-4d33-addd-41836308eb03', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:27:19.873' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'0c78804f-5206-4a63-b646-38a7745509a7', N'f127363f-9703-4290-9d36-16e9e5ffa4ac', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-08-12 23:35:19.090' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'3f60c0c0-740a-4d34-96cf-41985332e9f7', N'f46e33c8-9e0e-4c36-8af6-59f5f9530a4f', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:30:14.090' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'47574c12-4c02-44f6-bc78-42c992b23c0b', N'198ba134-4876-4b05-934e-7069b7f328c6', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:23:59.810' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'ef76f679-dd5d-42b6-a6a5-48969aee1c2a', N'791e06d8-3bc9-41f2-aa12-16ff50ad6cc4', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-09-03 10:21:47.013' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'b307b6a0-dea4-4439-951e-55e3f8ff6b36', N'4c6abaa7-e421-42f3-bbe3-d9621e8cdaec', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 16:00:52.723' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'62baee10-1cd0-4fa3-9fa9-5f599b7be9a8', N'669b5f6e-c750-4fff-8eb0-c2ee47343f3c', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:39:18.683' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'9778be0e-b02e-42d8-ac71-635379d5f420', N'a7eae7ab-0de4-4026-8da9-6529f8a1c3e2', N'40ff1844-c099-4061-98e0-cd6e544fcfd3', CAST(N'2017-06-14 15:15:02.230' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'04bf5ae8-3c48-4dcf-b7dd-682a5903900d', N'0198459e-2034-4533-b843-5d227ad20740', N'18fa4771-6f58-46a3-80d2-6f0f5e9972e3', CAST(N'2017-06-23 17:31:53.257' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'64419961-98d4-425e-bfe5-6e23497f7527', N'5225bbe0-ec73-4f36-8463-170514900da0', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-09-03 10:24:58.950' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'9667d457-67ab-4daa-bd16-72a6ae00bd1b', N'6aa9535b-9ad4-4682-b626-0eac3ceafd3c', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:52:20.980' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'7e770690-9786-4807-b294-9035a47b8197', N'677134de-203c-4efc-aa4b-8c02155bfb79', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:39:53.300' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'3493edb8-cdc1-44d0-9a1c-936e8f9842ec', N'c7b42924-cdc9-4fc3-8d33-99e6c6bd1c30', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:56:02.360' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'bf92551b-7cf2-440c-a334-94cf7da5912c', N'f4aee0e9-39e2-4ffa-8eb5-abcc2f12777f', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:30:59.517' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'2e407133-5781-4874-9cc0-9bcdcd8987ec', N'47e66bc8-cec7-4eeb-9ef4-a419d77dd38f', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:54:59.213' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'74d20a5a-cd60-4709-90ee-b86c46454c97', N'7ce414e9-acde-434e-a579-7ca5bfc45dfc', N'40ff1844-c099-4061-98e0-cd6e544fcfd3', CAST(N'2017-06-08 16:03:13.240' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'a8da1fff-81f6-4697-a9bb-c0029dfa107b', N'6770e239-4f60-4f34-90c2-b39ba2769fc5', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-09-02 21:32:35.470' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'e6b1436e-2630-4fba-8f1c-c3d3d1703b0d', N'9eca4ce1-3143-42f7-9a09-d02bc12b1af2', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:49:48.973' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'48749a9e-1d8a-4c40-a6b3-ce63d5f45a74', N'ee34ce20-bf42-4c95-b27d-84bfb04aa79e', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:55:48.313' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'f1865773-6960-45bd-af47-ea4d4c2ea039', N'677d4818-e2b9-41d3-9e2d-16fb5405f06d', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-09-03 10:22:15.747' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'7e51224d-bc6b-41fa-8df8-eaaedd166601', N'5b79a652-7f52-482a-bf43-5d304f9f3922', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:26:45.720' AS DateTime))
INSERT [dbo].[T_UsersRoles] ([uUsersRoles_ID], [uUsersRoles_UsersID], [uUsersRoles_RoleID], [dUsersRoles_CreateTime]) VALUES (N'4bd8a809-92a7-413e-914b-fb7c9ea6adbf', N'3474a663-4b2b-46c6-8620-3d1df9d08851', N'00000000-0000-0000-0000-000000000000', CAST(N'2017-06-08 15:57:40.147' AS DateTime))
ALTER TABLE [dbo].[Member] ADD  CONSTRAINT [DF_Member_Member_ID]  DEFAULT (newid()) FOR [Member_ID]
GO
ALTER TABLE [dbo].[Member] ADD  CONSTRAINT [DF_Member_Member_CreateTime]  DEFAULT (getdate()) FOR [Member_CreateTime]
GO
ALTER TABLE [dbo].[T_Number] ADD  CONSTRAINT [DF_T_Number_Number_ID]  DEFAULT (newid()) FOR [Number_ID]
GO
ALTER TABLE [dbo].[T_Number] ADD  CONSTRAINT [DF_T_Number_Number_CreateTime]  DEFAULT (getdate()) FOR [Number_CreateTime]
GO
/****** Object:  StoredProcedure [dbo].[GetNumber]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetNumber]
	@numfield varchar(50),--varchar(8000),         --字段名
    @tablename varchar(50)              --表名
AS
BEGIN
	DECLARE @Number int = 0
	select @Number=Number_Num from T_Number where Number_TableName=@tablename and Number_NumField=@numfield
    IF @Number=0 BEGIN
		insert into T_Number(Number_TableName,Number_NumField,Number_Num) values(@tablename,@numfield,1)
		select 1
	END
	ELSE BEGIN
		update T_Number set Number_Num = @Number +1 where Number_TableName=@tablename and Number_NumField=@numfield
		select (@Number +1)
	END
END


GO
/****** Object:  StoredProcedure [dbo].[PROC_SPLITPAGE]    Script Date: 2017/9/11 12:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------系统的--------------
CREATE  PROCEDURE [dbo].[PROC_SPLITPAGE]
    @SQL text,--varchar(8000),         --要执行的SQL语句
    @PAGE INT = 1,              --要显示的页码
    @PAGESIZE INT,              --每页的大小
    @PAGECOUNT INT = 0 OUT,     --总页数
    @RECORDCOUNT INT = 0 OUT    --总记录数
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @P1 INT

    EXEC SP_CURSOROPEN @P1 OUTPUT, @SQL, @SCROLLOPT = 1, @CCOPT = 1, @ROWCOUNT = @PAGECOUNT OUTPUT

    SET @RECORDCOUNT = @PAGECOUNT

    SELECT @PAGECOUNT=
        CEILING(1.0 * @PAGECOUNT / @PAGESIZE) , @PAGE = (@PAGE-1) * @PAGESIZE + 1

    EXEC SP_CURSORFETCH @P1, 16, @PAGE, @PAGESIZE 

    EXEC SP_CURSORCLOSE @P1
END


GO
