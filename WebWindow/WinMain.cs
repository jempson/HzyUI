﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebWindow
{
    public partial class WinMain : Form
    {
        public WinMain()
        {
            InitializeComponent();
        }

        private void WinMain_Load(object sender, EventArgs e)
        {
            WebKit.WebKitBrowser webkit = new WebKit.WebKitBrowser();
            webkit.Dock = DockStyle.Fill;
            
            this.Controls.Add(webkit);
            webkit.Navigate("http://47.92.123.194:8009/Admin/Login");
        }
    }
}
