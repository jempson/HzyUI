﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ExceptionHandle
{
    public class CustomErrorModel
    {
        public CustomErrorModel()
        {
            ErrorStatus = 0;
        }

        /// <summary>
        /// 异常模型
        /// </summary>
        /// <param name="ex">异常</param>
        public CustomErrorModel(Exception ex)
        {
            Success = true;
            ErrorMessage = ex.Message;
            status = MsgStatus.信息提示;
            ErrorData = ex.Data;
            ErrorSource = ex.Source;
            ErrorStackTrace = ex.StackTrace;
            ErrorTargetSite = ex.TargetSite != null ? ex.TargetSite.Name : null;
            this.JumpUrl = string.Empty;
        }

        /// <summary>
        /// 异常模型
        /// </summary>
        /// <param name="ex">异常</param>
        public CustomErrorModel(Exception ex, MsgStatus _status)
        {
            Success = true;
            ErrorMessage = ex.Message;
            status = _status;
            ErrorData = ex.Data;
            ErrorSource = ex.Source;
            ErrorStackTrace = ex.StackTrace;
            ErrorTargetSite = ex.TargetSite != null ? ex.TargetSite.Name : null;
            this.JumpUrl = string.Empty;
        }

        /// <summary>
        /// 违禁操作
        /// </summary>
        /// <param name="ex">异常(上下文)</param>
        public CustomErrorModel(string JumpUrl, string errorMessage = "系统监测到违法操作,系统将计时后退出")
        {
            if (string.IsNullOrEmpty(JumpUrl))
                throw new Exception("缺少JumpUrl参数");
            Success = true;
            ErrorMessage = errorMessage;
            status = MsgStatus.登录超时;
            ErrorData = "";
            ErrorSource = "";
            ErrorStackTrace = "";
            ErrorTargetSite = "";
            this.JumpUrl = JumpUrl;
        }

        /// <summary>
        /// 异常模型
        /// </summary>
        /// <param name="ex">异常(上下文)</param>
        public CustomErrorModel(ExceptionContext ex)
        {
            Success = true;
            ErrorMessage = ex.Exception.Message;
            //status = ex.HttpContext.Response.StatusCode.ToString();
            ErrorStatus = ex.HttpContext.Response.StatusCode;
            ErrorData = ex.Exception.Data;
            ErrorSource = ex.Exception.Source;
            ErrorStackTrace = ex.Exception.StackTrace;
            ErrorTargetSite = ex.Exception.TargetSite != null ? ex.Exception.TargetSite.Name : null;
            this.JumpUrl = string.Empty;
        }

        /// <summary>
        /// 请求是否成功
        /// </summary>
        public bool Success { set; get; }

        /// <summary>
        /// 错误消息
        /// </summary>
        public string ErrorMessage { set; get; }

        /// <summary>
        /// 错误状态码
        /// </summary>
        public MsgStatus status { set; get; }

        /// <summary>
        /// 错误状态码
        /// </summary>
        public int ErrorStatus { set; get; }

        /// <summary>
        /// 异常信息键值对集合
        /// </summary>
        public object ErrorData { set; get; }

        /// <summary>
        /// 异常对象名或应用程序
        /// </summary>
        public string ErrorSource { set; get; }

        /// <summary>
        /// 堆栈形式
        /// </summary>
        public string ErrorStackTrace { set; get; }

        /// <summary>
        /// 当前引发异常的方法
        /// </summary>
        public string ErrorTargetSite { set; get; }

        /// <summary>
        /// 跳转链接
        /// </summary>
        public string JumpUrl { set; get; }

    }


    /// <summary>
    /// 消息状态
    /// </summary>
    public enum MsgStatus
    {
        信息提示 = 200,
        登录超时 = 500,
        手动处理 = 501
    }


}
