﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using System.Collections;
using System.Data;
using Utility;
using DbFrame;
using DbFrame.Class;
using DAL;
using Model;

namespace BLL
{
    public class T_MenuBL
    {
        DBContext db = new DBContext();
        T_Menu t_menu = new T_Menu();

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="QuickConditions"></param>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public PagingEntity GetDataSource(Hashtable query, int pageindex, int pagesize)
        {
            return new T_MenuDA().GetDataSource(query, pageindex, pagesize);
        }

        /// <summary>
        /// 获取所有的菜单
        /// </summary>
        /// <returns></returns>
        public DataTable GetMenuByRoleID()
        {
            return new T_MenuDA().GetMenuByRoleID();
        }

        #region  左侧菜单
        public string GetSysMenu()
        {
            var menu_list = db.ToListByDt<T_Menu>(this.GetMenuByRoleID());
            StringBuilder sb = new StringBuilder();
            if (menu_list.Count > 0)
            {
                var parentList = menu_list.FindAll(item => item.uMenu_ParentID == null || Tools.getGuid(item.uMenu_ParentID) == Guid.Empty);
                foreach (var item in parentList)
                {
                    var childList = menu_list.FindAll(w => w.uMenu_ParentID != null && w.uMenu_ParentID == item.uMenu_ID);
                    if (childList.Count > 0)
                    {
                        //<li class="">
                        //<a class="has-first-menu has-arrow" href="#" aria-expanded="false"><i class=" fa fa-laptop"></i>&nbsp;&nbsp;<span>系统管理11133</span></a>
                        //</li>
                        sb.Append("<li title='[" + item.cMenu_Name + ">" + item.cMenu_Url + "]'>");
                        sb.Append(string.Format("<a class=\"has-first-menu has-arrow\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"{0}\"></i>&nbsp;&nbsp;<span>{1}</span></a>", item.cMenu_ICON, item.cMenu_Name));
                        GetChildMenu(menu_list, item.uMenu_ID.To_Guid(), sb);
                        sb.Append("</li>");
                    }
                    else
                    {
                        //<li mdata-id="/Admin/Home/HomePage/" mdata-url="/Admin/Home/HomePage/" mdata-title="首页" onclick="$web.Tab.Add(this);">
                        //<a href="javascript:void(0)" class="has-first-menu"><i class="fa fa-home"></i>&nbsp;&nbsp;<span>首页</span></a>
                        //</li>
                        sb.Append(string.Format("<li title='[" + item.cMenu_Name + ">" + item.cMenu_Url + "]' mdata-id=\"{0}\" mdata-url=\"{1}\" mdata-title=\"{2}\" onclick=\"$web.Tab.Add(this);\">", item.cMenu_Url, item.cMenu_Url, item.cMenu_Name));
                        sb.Append(string.Format("<a href=\"javascript:void(0)\" class=\"has-first-menu\"><i class=\"{0}\"></i>&nbsp;&nbsp;<span>{1}</span></a>", item.cMenu_ICON, item.cMenu_Name));
                        sb.Append("</li>");
                    }
                }
            }
            return sb.ToString();
        }
        public void GetChildMenu(List<T_Menu> menu_list, Guid id, StringBuilder sb)
        {
            var list = menu_list.FindAll(w => Tools.getGuid(w.uMenu_ParentID) == id);
            foreach (var item in list)
            {
                var childList = list.FindAll(w => w.uMenu_ParentID == item.uMenu_ID);
                if (list.IndexOf(item) == 0) sb.Append("<ul aria-expanded=\"false\">");
                if (childList.Count > 0)
                {
                    sb.Append("<li title='[" + item.cMenu_Name + ">" + item.cMenu_Url + "]'>");
                    sb.Append(string.Format("<a class=\"has-arrow\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"{0}\"></i>&nbsp;&nbsp;<span>{1}</span></a>", item.cMenu_ICON, item.cMenu_Name));
                    GetChildMenu(menu_list, item.uMenu_ID.To_Guid(), sb);
                    sb.Append("</li>");
                }
                else
                {
                    sb.Append(string.Format("<li title='[" + item.cMenu_Name + ">" + item.cMenu_Url + "]' mdata-id=\"{0}\" mdata-url=\"{1}\" mdata-title=\"{2}\" onclick=\"$web.Tab.Add(this);\">", item.cMenu_Url, item.cMenu_Url, item.cMenu_Name));
                    sb.Append(string.Format("<a href=\"javascript:void(0)\" ><i class=\"{0}\"></i>&nbsp;&nbsp;<span>{1}</span></a>", item.cMenu_ICON, item.cMenu_Name));
                    sb.Append("</li>");
                }
            }
            sb.Append("</ul>");
        }

        #endregion  左侧菜单

        #region  系统管理》菜单功能，角色功能  树的json处理

        /// <summary>
        /// 获取菜单和功能树
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetMenuAndFunctionTree()
        {
            var di = new Dictionary<string, object>();
            var tf_list = db.FindToList<T_Function>(null, " [iFunction_Number] asc");
            var list = new T_MenuDA().GetMenuAndFunctionTree();
            var tmf_list = db.FindToList<T_MenuFunction>(null);
            for (int i = 0; i < list.Count; i++)
            {
                string url = Tools.getString(list[i]["ur"]);
                string id = Tools.getGuidString(list[i]["id"]);
                if (!string.IsNullOrEmpty(url))
                {
                    tf_list.ForEach(x =>
                    {
                        di = new Dictionary<string, object>();
                        di.Add("name", x.cFunction_Name);
                        di.Add("id", x.uFunction_ID);
                        di.Add("pId", id);
                        di.Add("num", x.iFunction_Number);
                        di.Add("ur", "");
                        di.Add("tag", "fun");
                        if (list[i].ContainsKey("chkDisabled"))
                        {
                            di.Add("chkDisabled", true);
                        }
                        //判断该功能是否选中
                        var ischecked = tmf_list.Where(item =>
                            item.uMenuFunction_FunctionID == x.uFunction_ID && item.uMenuFunction_MenuID == Tools.getGuid(id)
                            ).FirstOrDefault();
                        if (ischecked == null)
                            di.Add("checked", false);
                        else
                            di.Add("checked", true);
                        list.Add(di);
                    });
                }
            }
            return list;
        }

        /// <summary>
        /// 获取角色对应的功能树
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetRoleMenuFunctionTree(string roleid)
        {
            var dic = new Dictionary<string, object>();
            //T_Function tf = new T_Function();
            //T_MenuFunction tmf = new T_MenuFunction();
            //T_RoleMenuFunction trmf = new T_RoleMenuFunction();
            var menu_list = db.FindToList<T_Menu>(null, " cMenu_Number desc ");
            var trmf_list = db.FindToList<T_RoleMenuFunction>(item => item.uRoleMenuFunction_RoleID == roleid.To_Guid());//角色菜单功能
            var tf_list = db.FindToList<T_Function>(null, " [iFunction_Number] asc");//功能
            var tmf_list = db.FindToList<T_MenuFunction>(null);//菜单功能

            var list = new List<Dictionary<string, object>>();
            var _paret_menu_list = menu_list.FindAll(item => item.uMenu_ParentID == null || item.uMenu_ParentID.Equals(Guid.Empty));
            for (int i = _paret_menu_list.Count - 1; i >= 0; i--)
            {
                var _child_menu_list = menu_list.FindAll(x => x.uMenu_ParentID != null && x.uMenu_ParentID.Equals(_paret_menu_list[i].uMenu_ID));
                //判断是否有子集
                if (_child_menu_list.Count() > 0)
                {
                    dic = new Dictionary<string, object>();
                    dic.Add("name", _paret_menu_list[i].cMenu_Name + "(" + _paret_menu_list[i].cMenu_Number + ")");
                    dic.Add("id", _paret_menu_list[i].uMenu_ID);
                    dic.Add("pId", _paret_menu_list[i].uMenu_ParentID);
                    dic.Add("num", _paret_menu_list[i].cMenu_Number);
                    dic.Add("ur", _paret_menu_list[i].cMenu_Url);
                    dic.Add("tag", null);
                    dic.Add("checked", false);
                    list.Add(dic);
                    this.FindChildMenu(menu_list, trmf_list, tf_list, tmf_list, _paret_menu_list[i], Tools.getGuid(roleid), list);
                }
                else
                {
                    if (tmf_list.FindAll(val => val.uMenuFunction_MenuID.Equals(_paret_menu_list[i].uMenu_ID)).Count() == 0)//判断该菜单是否有 勾选了功能 如果没有则删除
                    {
                        _paret_menu_list.RemoveAt(i);
                        continue;
                    }

                    dic = new Dictionary<string, object>();
                    dic.Add("name", _paret_menu_list[i].cMenu_Name + "(" + _paret_menu_list[i].cMenu_Number + ")");
                    dic.Add("id", _paret_menu_list[i].uMenu_ID);
                    dic.Add("pId", _paret_menu_list[i].uMenu_ParentID);
                    dic.Add("num", _paret_menu_list[i].cMenu_Number);
                    dic.Add("ur", _paret_menu_list[i].cMenu_Url);
                    dic.Add("tag", null);
                    dic.Add("checked", false);
                    list.Add(dic);

                    //找出该菜单下的功能和选中的功能
                    tf_list.ForEach(a =>
                    {
                        if (tmf_list.FindAll(val => val.uMenuFunction_FunctionID.Equals(a.uFunction_ID)
                            && val.uMenuFunction_MenuID.Equals(_paret_menu_list[i].uMenu_ID)).Count() > 0)
                        {
                            dic = new Dictionary<string, object>();
                            dic.Add("name", a.cFunction_Name);
                            dic.Add("id", a.uFunction_ID);
                            dic.Add("pId", _paret_menu_list[i].uMenu_ID);
                            dic.Add("num", a.iFunction_Number);
                            dic.Add("ur", null);
                            dic.Add("tag", "fun");
                            //判断该功能是否选中
                            var ischecked = trmf_list.FindAll(x => x.uRoleMenuFunction_FunctionID.Equals(a.uFunction_ID) && x.uRoleMenuFunction_MenuID.Equals(_paret_menu_list[i].uMenu_ID) && x.uRoleMenuFunction_RoleID.Equals(Tools.getGuid(roleid))).FirstOrDefault();
                            if (ischecked == null)
                                dic.Add("checked", false);
                            else
                                dic.Add("checked", true);
                            list.Add(dic);
                        }
                    });
                }
            }
            return list;
        }

        private void FindChildMenu(List<T_Menu> menu_list, List<T_RoleMenuFunction> trmf_list, List<T_Function> tf_list, List<T_MenuFunction> tmf_list, T_Menu menu, Guid roleid, List<Dictionary<string, object>> list)
        {
            var dic = new Dictionary<string, object>();

            var _paret_menu_list = menu_list.FindAll(item => item.uMenu_ParentID != null && item.uMenu_ParentID.Equals(menu.uMenu_ID));

            for (int i = _paret_menu_list.Count - 1; i >= 0; i--)
            {
                var _child_menu_list = menu_list.FindAll(x => x.uMenu_ParentID != null && x.uMenu_ParentID.Equals(_paret_menu_list[i].uMenu_ID));
                //判断是否有子集
                if (_child_menu_list.Count() > 0)
                {
                    dic = new Dictionary<string, object>();
                    dic.Add("name", _paret_menu_list[i].cMenu_Name + "(" + _paret_menu_list[i].cMenu_Number + ")");
                    dic.Add("id", _paret_menu_list[i].uMenu_ID);
                    dic.Add("pId", _paret_menu_list[i].uMenu_ParentID);
                    dic.Add("num", _paret_menu_list[i].cMenu_Number);
                    dic.Add("ur", _paret_menu_list[i].cMenu_Url);
                    dic.Add("tag", null);
                    dic.Add("checked", false);
                    list.Add(dic);
                    this.FindChildMenu(menu_list, trmf_list, tf_list, tmf_list, _paret_menu_list[i], Tools.getGuid(roleid), list);
                }
                else
                {
                    if (tmf_list.FindAll(val => val.uMenuFunction_MenuID.Equals(_paret_menu_list[i].uMenu_ID)).Count() == 0)//判断该菜单是否有 勾选了功能 如果没有则删除
                    {
                        _paret_menu_list.RemoveAt(i);
                        continue;
                    }

                    dic = new Dictionary<string, object>();
                    dic.Add("name", _paret_menu_list[i].cMenu_Name + "(" + _paret_menu_list[i].cMenu_Number + ")");
                    dic.Add("id", _paret_menu_list[i].uMenu_ID);
                    dic.Add("pId", _paret_menu_list[i].uMenu_ParentID);
                    dic.Add("num", _paret_menu_list[i].cMenu_Number);
                    dic.Add("ur", _paret_menu_list[i].cMenu_Url);
                    dic.Add("tag", null);
                    dic.Add("checked", false);
                    list.Add(dic);


                    //找出该菜单下的功能和选中的功能
                    tf_list.ForEach(a =>
                    {
                        if (tmf_list.FindAll(val => val.uMenuFunction_FunctionID.Equals(a.uFunction_ID)
                            && val.uMenuFunction_MenuID.Equals(_paret_menu_list[i].uMenu_ID)).Count() > 0)
                        {
                            dic = new Dictionary<string, object>();
                            dic.Add("name", a.cFunction_Name);
                            dic.Add("id", a.uFunction_ID);
                            dic.Add("pId", _paret_menu_list[i].uMenu_ID);
                            dic.Add("num", a.iFunction_Number);
                            dic.Add("ur", null);
                            dic.Add("tag", "fun");
                            //判断该功能是否选中
                            var ischecked = trmf_list.FindAll(x => x.uRoleMenuFunction_FunctionID.Equals(a.uFunction_ID) && x.uRoleMenuFunction_MenuID.Equals(_paret_menu_list[i].uMenu_ID) && x.uRoleMenuFunction_RoleID.Equals(Tools.getGuid(roleid))).FirstOrDefault();
                            if (ischecked == null)
                                dic.Add("checked", false);
                            else
                                dic.Add("checked", true);
                            list.Add(dic);
                        }
                    });
                }
            }
        }

        #endregion 系统管理》菜单功能，角色功能  树的json处理


    }
}
